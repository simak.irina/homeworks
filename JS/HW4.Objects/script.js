// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем,
// создать объект newUser со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(),
// который будет возвращать первую букву имени пользователя,
//  соединенную с фамилией пользователя, все в нижнем регистре
//   (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser().
// Вызвать у пользователя функцию getLogin().
// Вывести в консоль результат выполнения функции.

// Необязательное задание продвинутой сложности:
// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую.
// Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.



let firstName = prompt("Enter first name");

let lastName = prompt("Enter last name");

function createNewUser(name, secondName) {
  const newUser = {
    firstName: name,
    lastName: secondName,
    getLogin: function () {
     return (this.firstName.toLowerCase().slice(0, 1) + this.lastName.toLowerCase());
    }
  };
  return newUser;
}

let createUser = createNewUser(firstName, lastName);

console.log(createUser.getLogin())
