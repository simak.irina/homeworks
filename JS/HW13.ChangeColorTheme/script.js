const btnTheme = document.querySelector('.btn-theme');

function darkTheme() {
  document.body.classList.add('dark');
  btnTheme.classList.add('btn-theme-dark');
  localStorage.theme = 'dark';
}

function lightTheme() {
  document.body.classList.remove('dark')
  btnTheme.classList.remove('btn-theme-dark');
  localStorage.theme = 'light';
}

btnTheme.addEventListener('click', () => {
  if (document.body.classList.contains('dark'))
    lightTheme()
  else darkTheme()
});

if (localStorage.theme === 'dark') darkTheme()
