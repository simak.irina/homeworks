let slides = document.querySelectorAll('.image-to-show');
// console.log(slides)
let slider = [];
let autoTimer = "";

for (let i = 0; i < slides.length; i++) {
  slider[i] = slides[i].src;
  slides[i].remove();
}
// console.log(slider)

let step = 0;
let offset = 0;

function draw() {
  let img = document.createElement('img');
  img.src = slider[step];
  img.classList.add('image-to-show');
  img.style.left = offset * 400 + 'px';
  document.querySelector('.slider').appendChild(img);
  if (step + 1 == slider.length) {
    step = 0;
  } else {
    step++;
  }
  offset = 1;
}

function autoSlider() {
  let slides2 = document.querySelectorAll('.image-to-show');
  let offset2 = 0;
  for (let i = 0; i < slides2.length; i++) {
    slides2[i].style.left = offset2 * 400 - 400 + 'px';
    offset2++;
  }
  setTimeout(function () {
    slides2[0].remove();
    draw();
  }, 1000);
  autoTimer = setTimeout(autoSlider, 3000);
};

let btnStop = document.querySelector('.slider-stop');
btnStop.addEventListener('click', () => {
  clearTimeout(autoTimer);
  btnStop.disabled = true;
  btnStart.disabled = false;
});


let btnStart = document.querySelector('.slider-start')
btnStart.addEventListener('click', () => {
  btnStart.disabled = true
  btnStop.disabled = false;
  autoSlider();
});


draw()
draw()
autoSlider()
