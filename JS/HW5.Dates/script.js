// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.

// Технические требования:
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser())
//  и дополните ее следующим функционалом:

// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy)
//  и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени
// пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре)
//  и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).

// Вывести в консоль результат работы функции createNewUser(),
// а также функций getAge() и getPassword() созданного объекта.


function createNewUser() {

  let firstName = prompt("Enter first name");

  let secondName = prompt("Enter last name");

  let datebirth = prompt("Enter your date of birth - dd.mm.yyyy");

  let bday;

  bday = datebirth.substring(3, 6) + datebirth.substring(0, 3) + datebirth.substring(datebirth.length - 4, datebirth.length);

  const newUser = {
    firstName: firstName,
    lastName: secondName,
    dateOfbirth: bday,
    getAge: function () {
      let now = new Date();
      let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
      let todayBirth = new Date(this.dateOfbirth);

      const diff = today.getFullYear() - todayBirth.getFullYear();

      let age;

      if (todayBirth.getMonth() > today.getMonth() || todayBirth.getDate() > today.getDate() && todayBirth.getMonth() == today.getMonth()) {
        return age = diff - 1;
      } else {
        return age = diff;
      }
    },
    getPassword: function () {
      return (this.firstName.toUpperCase().slice(0, 1) + this.lastName.toLowerCase() + this.dateOfbirth.slice(6, 10));
    },
  }
  return newUser;
}

const user = createNewUser();

console.log(user.getAge());
console.log(user.getPassword());
