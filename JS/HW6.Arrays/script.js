// Реализовать функцию фильтра массива по указанному типу данных.

// Технические требования:
//   Написать функцию filterBy(), которая будет принимать в себя 2 аргумента.
// Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент,
// за исключением тех, тип которых был передан вторым аргументом.
// То есть, если передать массив['hello', 'world', 23, '23', null],
// и вторым аргументом передать 'string', то функция вернет массив[23, null].



// function filterBy(array, argType) {
//   let newArr = [];
//   array.forEach((el) => {
//     if (el !== argType) {
//       newArr.push(el);
//     };
//   });
//   return newArr;
// }


function filterBy(array, argType) {
  return array.filter((el) => {
   if (el === null) {
    return argType !== "null";
   } else return typeof el !== argType;
  });
 }

const arr = ['hello', 'world', 23, '23', null, undefined, false, {}]


// const nonObject = filterBy(arr, 'object');
// console.log(nonObject);

// const nonStrings = filterBy(arr, 'string');
// const nonNumbers = filterBy(arr, 'number');
// const nonBooleans = filterBy(arr, 'boolean');

const nonNull = filterBy(arr, 'null');

// console.log(nonStrings);
// console.log(nonNumbers);
// console.log(nonBooleans);

console.log(nonNull);
