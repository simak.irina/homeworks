// // Создать поле для ввода цены с валидацией.

// Технические требования:
//   При загрузке страницы показать пользователю поле ввода(input) с надписью Price.
// Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//   При фокусе на поле ввода - у него должна появиться рамка зеленого цвета.
// При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span,
//   в котором должен быть выведен текст:
//  Текущая цена: $ {значение из поля ввода}.
// Рядом с ним должна быть кнопка с крестиком(X).
// Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены.Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой,
//   под полем выводить фразу - Please enter correct price.span со значением при этом не создается.

let form = document.createElement("form");
form.setAttribute("action", "#")
form.style.cssText = `
margin-top: 20px;
padding: 15px 30px;
`
let label = document.createElement('label');
label.textContent = "Price ";
let input = document.createElement('input');

input.type = "text";
form.appendChild(label)
form.appendChild(input)

document.body.prepend(form)

input.style.cssText = `color: green;`

let text = document.createElement('p');
text.innerHTML = "Please enter correct price!";

function addFocus() {
  form.style.cssText = `
    margin-top: 20px;
    padding: 15px 30px;
    background-color: green;
    width: 250px;
  `
};

input.addEventListener('blur', () => {
  form.style.cssText = `
       margin-top: 20px;
       padding: 15px 30px;
`
});

function blurCreateSpan() {
  let span = document.createElement('span');
  span.classList.add('span-style');

  let btn = document.createElement('button');

  if (input.value <= 0 || isNaN(+input.value)) {
    input.style.cssText = `
    border: 2px solid red;
    `
    form.after(text);
    input.value = "";

  } else if (input.value > 0) {
    text.innerHTML = "";
    span.innerHTML += `Текущая цена: ${input.value} `;
    form.before(span);
    btn.textContent = "x";
    btn.style.cssText = `
      border-radius: 50% 50%;
      text-align: center;
      margin-left: 10px;
  `
    input.value = '';
    span.append(btn);
    form.style.cssText = ``;
    input.focus();

    btn.addEventListener('click', () => {
      span.remove();
      input.value = '';
    });
  }
};


input.addEventListener('blur', blurCreateSpan);
input.addEventListener('focus', addFocus);
