// В папке tabs лежит разметка для вкладок. Нужно,
//  чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки.
//  При этом остальной текст должен быть скрыт.
//  В комментариях указано, какой текст должен отображаться для какой вкладки.
// // Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// Нужно предусмотреть, что текст на вкладках может меняться,
// и что вкладки могут добавляться и удаляться.
// При этом нужно, чтобы функция, написанная в джаваскрипте,
//  из-за таких правок не переставала работать.


let tabNav = document.querySelectorAll('.tabs-title');
let tabContent = document.querySelectorAll('.tab');


tabNav.forEach((item) => {
  item.addEventListener('click', function () {
    let currentBtn = item;
    let tabName = currentBtn.getAttribute('data-tab');
    let currentTab = document.querySelector(tabName);

    if (!currentBtn.classList.contains('active')) {
      tabNav.forEach((elem) => {
        elem.classList.remove("active");
      })
      tabContent.forEach((elem) => {
        elem.classList.remove("active");
      });
      currentBtn.classList.add('active');
      currentTab.classList.add('active');
    }
  })

})
