let btnEye = document.querySelectorAll('.icon-password');

btnEye.forEach((elem) => {
  elem.addEventListener('click', toggleType)
});

function toggleType() {
  console.log("ll");
  let input = this.closest(".input-wrapper").querySelector('.password')
  let faEye = document.querySelector('.fa-eye')
  let faEyeSlash = document.querySelector('.fa-eye-slash')

  if (input.type === "password") {
    input.type = 'text';
    faEye.style.cssText = `display: block;`
    faEyeSlash.style.cssText = `display: none;`
  } else {
    input.type = 'password';
    faEye.style.cssText = `display: none;`
    faEyeSlash.style.cssText = `display: block;`
  }
}

let password = document.querySelector('.form-pass')
let passwordConf = document.querySelector('.form-pass-confirm')



let button = document.querySelector('button');

button.addEventListener('click', (event) => {
  event.preventDefault();

  if (password.value !== passwordConf.value) {
    let span = document.createElement('span');
    span.style.cssText = `color: red`;
    span.innerHTML = "Нужно ввести одинаковые значения!";
    passwordConf.after(span);
  } else
    alert('You are welcome!');
});
