// Создать функцию, которая будет принимать на вход массив
//  и опциональный второй аргумент parent - DOM-элемент,
//  к которому будет прикреплен список
//  (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования
//  контента списка перед выведением его на страницу;



const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function showList(arr, el = document.querySelector('body')) {

  const ulList = document.createElement('ul');
  const newArray = arr.map((elem) => {
    let liList = `<li>${elem}</li>`;
    return liList;
  })
  ulList.innerHTML = newArray.join('');
  el.prepend(ulList);
  console.log(ulList)
  return ulList;
}


showList(array)


// const newArr = array.map((elem) => {
//   const li = document.createElement('li');
//   li.innerText = elem;
//   // console.log(elem)
//   newList.append(li);
// })
// document.body.prepend(newList);
