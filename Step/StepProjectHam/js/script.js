let tabNav = document.querySelectorAll('.tab-title')
let tabContent = document.querySelectorAll('.tab')

tabNav.forEach((item) => {
  item.addEventListener('click', function () {
    let currentBtn = item;
    let tabName = currentBtn.getAttribute('data-tab');
    let currentTab = document.querySelector(tabName);

    if (!currentBtn.classList.contains('active')) {
      tabNav.forEach((elem) => {
        elem.classList.remove("active");
      })
      tabContent.forEach((elem) => {
        elem.classList.remove("active");
      });
      currentBtn.classList.add('active');
      currentTab.classList.add('active');
    }
  })

})

let tabName = document.querySelectorAll('.tab-name')
let tabImagesContent = document.querySelectorAll('.tab-images')

tabName.forEach((item) => {
  item.addEventListener('click', function () {
    let currentBtn = item;
    let tabNameImg = currentBtn.getAttribute('data-tab');
    let currentTab = document.querySelector(tabNameImg);

    if (!currentBtn.classList.contains('active-images')) {
      tabName.forEach((elem) => {
        elem.classList.remove("active-images");
      })
      tabImagesContent.forEach((elem) => {
        elem.classList.remove("active-images");

      });
      currentBtn.classList.add('active-images');
      currentTab.classList.add('active-images');
    }
  })

})

$(document).ready(function () {
  $('.slider').slick({
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 3,
    speed: 500,
    easing: 'ease',
    asNavFor: ".slider-second",
    focusOnSelect: true,
    autoplay: true
  });
  $('.slider-second').slick({
    arrows: false,
    fade: true,
    asNavFor: ".slider",
  })

  $('.container-grid').masonry({
    itemSelector: '.grid-item',
    gutter: '.gutter-size',
  });
  $('.container-grid-mini').masonry({
    itemSelector: '.item-mini-grid',
    gutter: '.mini-gutter-size',
  })
  $('.grid3').masonry({
    itemSelector: '.item-grid3',
    gutter: '.gutter-size-grid3'
  })
  $('.btn-load-first').on('click', function (e) {
    e.preventDefault();
    $('.img-none:hidden').slice(0, 12).slideDown();

    if ($('.img-none:hidden').length == 0) {
      $('.btn-load-first').remove();
    }

  });
})
