"use strict"

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }
  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }
  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, sallary, lang) {
    super(name, age, sallary);
    this.lang = lang;
  }
  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary * 3;

  }

}

const employee = new Employee("ira", 20, 10000)
console.log(employee)

const programmer = new Programmer("irina", 21, 10000, "en")
console.log(programmer)


const programmer1 = new Programmer("irina", 21, 10000, "en")
console.log(programmer1)

const programmer2 = new Programmer("irina", 21, 10000, "en")
console.log(programmer2)




